/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.lab4;

/**
 *
 * @author DELL
 */
public class Board {
    private char[][] board = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
    private Player player1, player2, player;
    private int row, col;
    private int count;

     public Board(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.player = player1;
    }

    public char[][] getBoard() {
        return board;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean setRowCol(int row, int col) {
        if (board[row - 1][col - 1] == '_') {
            board[row - 1][col - 1] = player.getSymbol();
            this.row = row;
            this.col = col;
            this.count++;
            return true;
        }
        return false;
    }
    
    public boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        }
        if (checkCol()) {
            saveWin();
            return true;
        }
        if (checkX1()) {
            saveWin();
            return true;
        }
        if (checkX2()) {
            saveWin();
            return true;
        }
        return false;
    }
    
    public boolean checkDraw() {
        if (count==9) {
            player1.draw();
            player2.draw();
            return true;
        }
        return false;
    }


     private boolean checkRow() {
        return board[row - 1][0] != '_' && board[row - 1][0] == board[row - 1][1]
                && board[row - 1][0] == board[row - 1][2];
    }

     private boolean checkCol() {
        return board[0][col - 1] != '_' && board[0][col - 1] == board[1][col - 1]
                && board[0][col - 1] == board[2][col - 1];
    }

     private boolean checkX1() {
        if (board[0][0] == player.getSymbol() && board[1][1] == player.getSymbol() && board[2][2] == player.getSymbol()) {
            return true;
        }
        return false;
    }
     
    private boolean checkX2() {
        if (board[0][2] == player.getSymbol() && board[1][1] == player.getSymbol() && board[2][0] == player.getSymbol()) {
            return true;
        }
        return false;
    }

    private void saveWin() {
        if (player == player1) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    void switchPlayer() {
        if (player == player1) {
            player = player2;
        } else {
            player = player1;
        }
    }
}
